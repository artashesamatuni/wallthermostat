#ifndef _SMART_NTP_H
#define _SMART_NTP_H

#include <Arduino.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include "RTClib.h"

extern RTC_Millis rtc;






extern void ntp_setup();
unsigned long sendNTPpacket(IPAddress& address);
void ntp_restart();
extern void time_loop(void);

#endif // _SMART_NTP_H
