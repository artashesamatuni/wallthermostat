#ifndef _GENERAL_H
#define _GENERAL_H

// -------------------------------------------------------------------
// General support code used by all modules
// -------------------------------------------------------------------

// Uncomment to use hardware UART 1 for debug else use UART 0
//#define DEBUG_SERIAL1

#ifdef DEBUG_SERIAL1
  #define DEBUG Serial1
#else
  #define DEBUG Serial
#endif


#define TEXTIFY(A) #A
#define ESCAPEQUOTE(A) TEXTIFY(A)

#ifdef ENABLE_DEBUG

  #ifndef DEBUG_PORT
    #define DEBUG_PORT Serial1
  #endif

  #define DEBUG_BEGIN(speed)  DEBUG_PORT.begin(speed)

  #define DBUGF(format, ...)  DEBUG_PORT.printf(format "\n", ##__VA_ARGS__)
  #define DBUG(...)           DEBUG_PORT.print(__VA_ARGS__)
  #define DBUGLN(...)         DEBUG_PORT.println(__VA_ARGS__)

#else

  #define DEBUG_BEGIN(speed)
  #define DBUGF(...)
  #define DBUG(...)
  #define DBUGLN(...)

#endif // DEBUG













#define sensor_pin 2

//LCD
#define TFT_CS         15
#define TFT_RST        5
#define TFT_DC         4



#endif // _GENERAL_H
