#ifndef _SMART_LCD_H
#define _SMART_LCD_H



#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
//#include <Adafruit_TFTLCD.h>
#include <Fonts/FreeSerif9pt7b.h>
#include <SPI.h>




extern void lcd_init();
extern void lcd_clean();
extern void lcd_update(bool stat);
extern void lcd_frame(String label);
extern void lcd_time();
#endif // _SMART_LCD_H
