#ifndef _SMART_OTA_H
#define _SMART_OTA_H

// -------------------------------------------------------------------
// Support for updating the fitmware os the ESP8266
// -------------------------------------------------------------------

#include <Arduino.h>
#include <ESP8266httpUpdate.h>


void ota_setup();
void ota_loop();
String ota_get_latest_version();
t_httpUpdate_return ota_http_update();

#endif // _SMART_OTA_H
