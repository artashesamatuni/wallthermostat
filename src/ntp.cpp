#include "general.h"
#include "config.h"
#include "ntp.h"

WiFiUDP udp;

IPAddress timeServerIP(ntp_ip[0], ntp_ip[1], ntp_ip[2], ntp_ip[3]);
unsigned int localPort = 2390;      // local port to listen for UDP packets
const int NTP_PACKET_SIZE = 48;
byte packetBuffer[ NTP_PACKET_SIZE];
byte  old_h, old_m, old_s, old_dy, old_dm, old_dd;






void ntp_restart()
{
  ntp_setup();
}

void ntp_setup()
{
  udp.begin(localPort);
  sendNTPpacket(timeServerIP);
  delay(1000);
  int cb = udp.parsePacket();
  udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

  unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
  unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
  unsigned long secsSince1900 = highWord << 16 | lowWord;
  const unsigned long seventyYears = 2208988800UL;
  unsigned long epoch = secsSince1900 - seventyYears + ntp_tz * 3600;//TZ
  rtc.begin(DateTime(F(__DATE__), F(__TIME__)));
  rtc.adjust(epoch);
}




unsigned long sendNTPpacket(IPAddress& address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}



void time_loop(void) {
  DateTime now = rtc.now();
  c_h = now.hour();
  c_m = now.minute();
  c_s = now.second();
  c_dd = now.day();
  c_dm = now.month();
  c_dy = now.year();
}
