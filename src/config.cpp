#include "general.h"
#include "config.h"

#include <Arduino.h>
#include <EEPROM.h>                   // Save config settings

// Wifi Network Strings
String esid = "";
String epass = "";

// Web server authentication (leave blank for none)
String www_username = "";
String www_password = "";

// MQTT Settings
String mqtt_server = "";
String mqtt_topic = "";
String mqtt_user = "";
String mqtt_pass = "";
String mqtt_feed_prefix = "";

//TIME
uint8_t ntp_tz;
uint8_t ntp_ip[4];
int c_dy, c_dm, c_dd, c_h, c_m, c_s;

//Sensor
float t_sensor;

#define EEPROM_ESID_SIZE                  32
#define EEPROM_EPASS_SIZE                 64
#define EEPROM_MQTT_SERVER_SIZE           45
#define EEPROM_MQTT_TOPIC_SIZE            32
#define EEPROM_MQTT_USER_SIZE             32
#define EEPROM_MQTT_PASS_SIZE             64
#define EEPROM_MQTT_FEED_PREFIX_SIZE      10
#define EEPROM_WWW_USER_SIZE              16
#define EEPROM_WWW_PASS_SIZE              16
#define EEPROM_NTP_IP_SIZE                4
#define EEPROM_NTP_TZ_SIZE                1
#define EEPROM_SIZE                       512

#define EEPROM_ESID_START                 0
#define EEPROM_ESID_END                   (EEPROM_ESID_START + EEPROM_ESID_SIZE)
#define EEPROM_EPASS_START                EEPROM_ESID_END
#define EEPROM_EPASS_END                  (EEPROM_EPASS_START + EEPROM_EPASS_SIZE)
#define EEPROM_MQTT_SERVER_START          EEPROM_EPASS_END
#define EEPROM_MQTT_SERVER_END            (EEPROM_MQTT_SERVER_START + EEPROM_MQTT_SERVER_SIZE)
#define EEPROM_MQTT_TOPIC_START           EEPROM_MQTT_SERVER_END
#define EEPROM_MQTT_TOPIC_END             (EEPROM_MQTT_TOPIC_START + EEPROM_MQTT_TOPIC_SIZE)
#define EEPROM_MQTT_USER_START            EEPROM_MQTT_TOPIC_END
#define EEPROM_MQTT_USER_END              (EEPROM_MQTT_USER_START + EEPROM_MQTT_USER_SIZE)
#define EEPROM_MQTT_PASS_START            EEPROM_MQTT_USER_END
#define EEPROM_MQTT_PASS_END              (EEPROM_MQTT_PASS_START + EEPROM_MQTT_PASS_SIZE)
#define EEPROM_MQTT_FEED_PREFIX_START     EEPROM_MQTT_PASS_END
#define EEPROM_MQTT_FEED_PREFIX_END       (EEPROM_MQTT_FEED_PREFIX_START + EEPROM_MQTT_FEED_PREFIX_SIZE)
#define EEPROM_WWW_USER_START             EEPROM_MQTT_FEED_PREFIX_END
#define EEPROM_WWW_USER_END               (EEPROM_WWW_USER_START + EEPROM_WWW_USER_SIZE)
#define EEPROM_WWW_PASS_START             EEPROM_WWW_USER_END
#define EEPROM_WWW_PASS_END               (EEPROM_WWW_PASS_START + EEPROM_WWW_PASS_SIZE)
#define EEPROM_NTP_IP_START               EEPROM_WWW_PASS_END
#define EEPROM_NTP_IP_END                 (EEPROM_NTP_IP_START + EEPROM_NTP_IP_SIZE)
#define EEPROM_NTP_TZ_START               EEPROM_NTP_IP_END
#define EEPROM_NTP_TZ_END                 (EEPROM_NTP_TZ_START + EEPROM_NTP_TZ_SIZE)
// -------------------------------------------------------------------
// Reset EEPROM, wipes all settings
// -------------------------------------------------------------------
void ResetEEPROM() {
  //DEBUG.println("Erasing EEPROM");
  for (int i = 0; i < EEPROM_SIZE; ++i) {
    EEPROM.write(i, 0);
    //DEBUG.print("#");
  }
  EEPROM.commit();
}

void EEPROM_read_string(int start, int count, String & val) {
  for (int i = 0; i < count; ++i) {
    byte c = EEPROM.read(start + i);
    if (c != 0 && c != 255)
      val += (char) c;
  }
}

void EEPROM_write_string(int start, int count, String val) {
  for (int i = 0; i < count; ++i) {
    if (i < val.length()) {
      EEPROM.write(start + i, val[i]);
    } else {
      EEPROM.write(start + i, 0);
    }
  }
}

byte EEPROM_read_byte(int start) {

  return EEPROM.read(start);
}
void EEPROM_write_byte(int start, byte val) {
  EEPROM.write(start, val);
}

// -------------------------------------------------------------------
// Load saved settings from EEPROM
// -------------------------------------------------------------------
void config_load_settings()
{
  EEPROM.begin(EEPROM_SIZE);

  // Load WiFi values
  EEPROM_read_string(EEPROM_ESID_START, EEPROM_ESID_SIZE, esid);
  EEPROM_read_string(EEPROM_EPASS_START, EEPROM_EPASS_SIZE, epass);

  esid = "SkyNet";
  epass = "terminal";

  // MQTT settings
  EEPROM_read_string(EEPROM_MQTT_SERVER_START, EEPROM_MQTT_SERVER_SIZE, mqtt_server);
  EEPROM_read_string(EEPROM_MQTT_TOPIC_START, EEPROM_MQTT_TOPIC_SIZE, mqtt_topic);
  EEPROM_read_string(EEPROM_MQTT_FEED_PREFIX_START, EEPROM_MQTT_FEED_PREFIX_SIZE, mqtt_feed_prefix);
  EEPROM_read_string(EEPROM_MQTT_USER_START, EEPROM_MQTT_USER_SIZE, mqtt_user);
  EEPROM_read_string(EEPROM_MQTT_PASS_START, EEPROM_MQTT_PASS_SIZE, mqtt_pass);

  // Web server credentials
  EEPROM_read_string(EEPROM_WWW_USER_START, EEPROM_WWW_USER_SIZE, www_username);
  EEPROM_read_string(EEPROM_WWW_PASS_START, EEPROM_WWW_PASS_SIZE, www_password);

  for (int i = 0; i < 4; i++)
  {
    ntp_ip[i] = EEPROM_read_byte(EEPROM_NTP_IP_START + i);
  }
  ntp_tz = EEPROM_read_byte(EEPROM_NTP_TZ_START);
  DEBUG.println("Load Configuration...................[OK]");
}


void config_save_mqtt(String server, String topic, String prefix, String user, String pass)
{
  mqtt_server = server;
  mqtt_topic = topic;
  mqtt_feed_prefix = prefix;
  mqtt_user = user;
  mqtt_pass = pass;

  // Save MQTT server max 45 characters
  EEPROM_write_string(EEPROM_MQTT_SERVER_START, EEPROM_MQTT_SERVER_SIZE, mqtt_server);

  // Save MQTT topic max 32 characters
  EEPROM_write_string(EEPROM_MQTT_TOPIC_START, EEPROM_MQTT_TOPIC_SIZE, mqtt_topic);

  // Save MQTT topic separator max 10 characters
  EEPROM_write_string(EEPROM_MQTT_FEED_PREFIX_START, EEPROM_MQTT_FEED_PREFIX_SIZE, mqtt_feed_prefix);

  // Save MQTT username max 32 characters
  EEPROM_write_string(EEPROM_MQTT_USER_START, EEPROM_MQTT_USER_SIZE, mqtt_user);

  // Save MQTT pass max 64 characters
  EEPROM_write_string(EEPROM_MQTT_PASS_START, EEPROM_MQTT_PASS_SIZE, mqtt_pass);

  EEPROM.commit();
}

void config_save_admin(String user, String pass)
{
  www_username = user;
  www_password = pass;

  EEPROM_write_string(EEPROM_WWW_USER_START, EEPROM_WWW_USER_SIZE, user);
  EEPROM_write_string(EEPROM_WWW_PASS_START, EEPROM_WWW_PASS_SIZE, pass);

  EEPROM.commit();
}

void config_save_time(String a, String b, String c, String d, String tz)
{
  ntp_ip[0] = a.toInt();
  ntp_ip[1] = b.toInt();
  ntp_ip[2] = c.toInt();
  ntp_ip[3] = d.toInt();
  ntp_tz = tz.toInt();
  for (int i = 0; i < 4; i++)
  {
    EEPROM_write_byte(EEPROM_NTP_IP_START + i, ntp_ip[i]);
  }
  EEPROM_write_byte(EEPROM_NTP_TZ_START, ntp_tz);
  EEPROM.commit();
}

void config_save_wifi(String qsid, String qpass)
{
  esid = qsid;
  epass = qpass;

  EEPROM_write_string(EEPROM_ESID_START, EEPROM_ESID_SIZE, qsid);
  EEPROM_write_string(EEPROM_EPASS_START, EEPROM_EPASS_SIZE, qpass);

  EEPROM.commit();
}

void config_reset()
{
  ResetEEPROM();
  EEPROM.commit();
}
