#ifndef _SMART_INPUT_H
#define _SMART_INPUT_H

#include <Arduino.h>

// -------------------------------------------------------------------
// Support for reading input
// -------------------------------------------------------------------

extern String last_datastr;
extern String input_string;

// -------------------------------------------------------------------
// Read input sent via the web_server or serial.
//
// data: if true is returned data will be updated with the new line of
//       input
// -------------------------------------------------------------------
extern boolean input_get(String& data);

#endif // _SMART_INPUT_H
