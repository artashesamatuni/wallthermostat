#include "general.h"
#include <OneWire.h>


OneWire  ds(sensor_pin);

bool present = false;
unsigned long StartTime;
unsigned long CurrentTime;
unsigned long ElapsedTime;
byte type_s;
byte data[12];
byte addr[8];
float celsius;



void init_t(byte *addr) {
  if ( !ds.search(addr)) {
    ds.reset_search();
  }
}


void get_type(byte *addr) {
  byte type;
  switch (addr[0]) {
    case 0x10:
      type_s = 1;
      break;
    case 0x28:
      type_s = 0;
      break;
    case 0x22:
      type_s = 0;
      break;
    default:
      return;
  }
}


bool check_t(byte *addr) {
  if (OneWire::crc8(addr, 7) != addr[7]) {
    return true;
  }
  else
  {
    return false;
  }
}



float convert_raw(byte type, byte * data) {
  float t;
  int16_t raw = (data[1] << 8) | data[0];
  if (type) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  t = (float)raw / 16.0;
  return t;
}

void get_temperature(float &sensor) {
  float t;
  if (!present) {
    init_t(addr);
    get_type(addr);
    present = true;
    StartTime = millis();
  }
  else
  {
    CurrentTime = millis();
    ElapsedTime = CurrentTime - StartTime;
    if (ElapsedTime >= 1000)
    {
      StartTime = CurrentTime;
      ds.reset();
      ds.select(addr);
      ds.write(0xBE);
      for (int i = 0; i < 9; i++) {
        data[i] = ds.read();
      }
    }
    else
    {
      ds.reset();
      ds.select(addr);
      ds.write(0x44, 1);
    }
  }
  sensor = convert_raw(type_s, data);
}
