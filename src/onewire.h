#ifndef _SMART_ONEWIRE_H
#define _SMART_ONEWIRE_H



#include <Arduino.h>


extern void get_temperature(float &sensor);

void get_type(byte *addr);
void init_t(byte *addr);
bool check_t(byte *addr);
float convert_raw(byte type, byte *data);


#endif // _SMART_ONEWIRE_H
