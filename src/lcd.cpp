#include "general.h"
#include "config.h"
#include "lcd.h"



Adafruit_ST7735 screen = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

bool lcdUpdate;


void lcd_init() {
  screen.initR(INITR_BLACKTAB);
  screen.setRotation(1);
  screen.fillScreen(ST77XX_BLACK);
  DEBUG.println("LCD..................................[OK]");
}

void lcd_clean() {
  screen.fillScreen(ST77XX_BLACK);
}

void lcd_update(bool stat) {
  lcdUpdate = stat;
}



void lcd_time() {
  if (lcdUpdate) {
    String s = "";
    screen.setCursor(55, 55);
    screen.setTextColor(ST77XX_YELLOW);
    screen.setTextSize(1);
    if (c_h < 10)
      s += "0";
    s += String(c_h);
    if (c_s % 2 == 1)
      s += ":";
    else
      s += " ";
    if (c_m < 10)
      s += "0";
    s += String(c_m);
    screen.print(s);
  }
}

void lcd_frame(String label) {
  if (lcdUpdate) {
    screen.drawRect(0, 0, 160, 128, ST77XX_YELLOW);
    screen.drawRect(0, 0, 160, 20, ST77XX_YELLOW);
    screen.fillRect(1, 1, 158, 18, ST77XX_BLUE);
    screen.setCursor(1, 9);
    screen.setTextColor(ST77XX_YELLOW);
    screen.setTextSize(0);
    screen.setFont(&FreeSerif9pt7b);
    screen.print(label);
    screen.setFont();
    //lcd_time();
  }
}
