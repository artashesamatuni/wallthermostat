#include "general.h"
#include "config.h"
#include "wifi.h"
#include "web_server.h"
#include "ota.h"
#include "input.h"
#include "mqtt.h"
#include "ntp.h"
#include "onewire.h"
#include "lcd.h"

// -------------------------------------------------------------------
// SETUP
// -------------------------------------------------------------------




void setup() {
  delay(2000);
  Serial.begin(9600);
#ifdef DEBUG_SERIAL1
  Serial1.begin(115200);
#endif
  DEBUG.println();
  DEBUG.println("-------------------------------------------------------");
  DEBUG.println("Firmware: " + currentfirmware);
  DEBUG.print("Serial: ");
  DEBUG.println(ESP.getChipId());
  DEBUG.println();
  DEBUG.println("SSID: " + esid);
  DEBUG.println("PASS: " + epass);

  // Read saved settings from the config
  config_load_settings();

  // Initialise the WiFi
  wifi_setup();

  // Bring up the web server
  web_server_setup();

  // Start the OTA update systems
  ota_setup();

  DEBUG.println("Server started");
  ntp_setup();
  lcd_init();
  lcd_update(true);
} // end setup

// -------------------------------------------------------------------
// LOOP
// -------------------------------------------------------------------
void loop()
{
  time_loop(); //update time
  ota_loop();
  web_server_loop();
  wifi_loop();

 



  
  get_temperature(t_sensor);
  lcd_frame("Smart-T");

  lcd_time();
  lcd_update(false);





  String input = "";
  boolean gotInput = input_get(input);
/*
  if (wifi_mode == WIFI_MODE_STA || wifi_mode == WIFI_MODE_AP_AND_STA)
  {
    if (mqtt_server != 0)
    {
      mqtt_loop();
      if (gotInput) {
        mqtt_publish(input);
      }
    }
  }
  */
  delay(1);
} // end loop
